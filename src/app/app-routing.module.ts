// IMPORT NECESARIOS
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListarRecursosComponent } from './components/recepcion/listar-recursos/listar-recursos.component';
import { LoginComponent } from './components/login/login.component';
import { BienvenidaComponent } from './components/bienvenida/bienvenida.component';
import { PresentacionRecursoComponent } from './components/recepcion/presentacion-recurso/presentacion-recurso.component';
import { DefinicionRecursoComponent } from './components/recepcion/definicion-recurso/definicion-recurso.component';
import { GestionarActaRecursoComponent } from './components/recepcion/gestionar-acta-recurso/gestionar-acta-recurso.component';
import { ImpugnarGastosComponent } from './components/recepcion/impugnar-gastos/impugnar-gastos.component';
import { ArgumentoGastosComponent } from './components/recepcion/argumento-gastos/argumento-gastos.component';
import { AntecedentesComponent } from './components/recepcion/antecedentes/antecedentes.component';
import { ArgumentoEscritoComponent } from './components/recepcion/argumento-escrito/argumento-escrito.component';
import { SubirEscritoComponent } from './components/recepcion/subir-escrito/subir-escrito.component';


const appRoutes: Routes = [
  {path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent },
  { path: 'logout/:sure', component: LoginComponent },
  { path: 'bienvenida', component: BienvenidaComponent },
  { path: 'listar-recursos', component: ListarRecursosComponent },
  { path: 'presentacion-recurso', component: PresentacionRecursoComponent },
  { path: 'definicion-recurso', component: DefinicionRecursoComponent },
  { path: 'gestionar-acta-recurso', component: GestionarActaRecursoComponent },
  { path: 'gastos', component: ImpugnarGastosComponent },
  { path: 'argumento-gastos', component: ArgumentoGastosComponent },
  { path: 'antecedentes', component: AntecedentesComponent },
  { path: 'argumento-escrito', component: ArgumentoEscritoComponent },
  { path: 'subir-escrito', component: SubirEscritoComponent }
];

// EXPORTAR CONFIGURACIÓN
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
