import { Component, OnInit } from '@angular/core';
import { global } from '../../../services/global';

@Component({
  selector: 'app-impugnar-gastos',
  templateUrl: './impugnar-gastos.component.html',
  styleUrls: ['./impugnar-gastos.component.css']
})
export class ImpugnarGastosComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  public status: string;
  public ruta;
  public action;
  isNonTrade: boolean = false;
  checkAllNonTrades: boolean = false;

  constructor() { 
    this.ruta = global.ruta;
  }

  postsRemuneracion = [
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "cuenta": "Sueldo base",
      "n_cuenta": "410101",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 392.416",
      "montoimpugnado": "$ 392.416"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "cuenta": "Incremento % zona",
      "n_cuenta": "410104",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 78.483",
      "montoimpugnado": "$ 78.483"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "cuenta": "Seguro de accidente del trabajo",
      "n_cuenta": "410401",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 4.474",
      "montoimpugnado": "$ 4.474"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "cuenta": "Seguro de cesantía",
      "n_cuenta": "410402",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 14.127",
      "montoimpugnado": "$ 14.127"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "cuenta": "Seguro de invalidez y sobrevivencia",
      "n_cuenta": "410403",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 5.415",
      "montoimpugnado": "$ 5.415"
    }


  ];

  postsBoletas = [
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "n_doc": "51681227",
      "fecha_doc": "12/01/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 20.000",
      "montoimpugnado": "$ 20.000"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "n_doc": "42467002",
      "fecha_doc": "18/08/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 25.000",
      "montoimpugnado": "$ 25.000"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "n_doc": "42842193",
      "fecha_doc": "14/09/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 30.000",
      "montoimpugnado": "$ 30.000"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "n_doc": "42411277",
      "fecha_doc": "18/08/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 30.000",
      "montoimpugnado": "$ 30.000"
    },
    {
      "selected": false,
      "id": 123,
      "RBD": "4791",
      "trabajador": "PRIME LTDA",
      "periodo": "1",
      "mes": "",
      "subvencion": "",
      "n_doc": "49185031",
      "fecha_doc": "04/11/2015",
      "detalle": "Combustible",
      "cuenta": "",
      "n_cuenta": "",
      "fundamentacion": "",
      "motivo": "",
      "montono": "$ 30.000",
      "montoimpugnado": "$ 30.000"
    }


  ];

  ngOnInit() {
    // Conseguir elemento
    this.action = localStorage.getItem("action");

    this.dtOptions = {
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
      }
    };
  }

  onSubmit(form){
    this.status = "success";
  }

/*   changeTradesByCategory(event) {

    if (event.target.name == 'nontrades') {
      this.isNonTrade = true
    }

    if (this.isNonTrade && this.checkAllNonTrades) {
      event.target.checked = true
    }
  } */
  allNonTrades(event) {
    const checked = event.target.checked;
    this.postsRemuneracion.forEach(item => item.selected = checked);
    this.postsBoletas.forEach(item => item.selected = checked);
  }

}
