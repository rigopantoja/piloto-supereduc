import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpugnarGastosComponent } from './impugnar-gastos.component';

describe('ImpugnarGastosComponent', () => {
  let component: ImpugnarGastosComponent;
  let fixture: ComponentFixture<ImpugnarGastosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpugnarGastosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpugnarGastosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
