import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionarActaRecursoComponent } from './gestionar-acta-recurso.component';

describe('GestionarActaRecursoComponent', () => {
  let component: GestionarActaRecursoComponent;
  let fixture: ComponentFixture<GestionarActaRecursoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionarActaRecursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionarActaRecursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
