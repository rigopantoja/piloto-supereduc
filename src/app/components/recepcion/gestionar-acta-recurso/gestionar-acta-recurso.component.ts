import { Component, OnInit, ɵConsole } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { global } from '../../../services/global';

@Component({
  selector: 'app-gestionar-acta-recurso',
  templateUrl: './gestionar-acta-recurso.component.html',
  styleUrls: ['./gestionar-acta-recurso.component.css'],
  providers: [UserService]
})
export class GestionarActaRecursoComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  public status: string;
  public perfil: string;
  public p;
  public ruta;
  public action;
  posts = [];

  constructor(
    public _userService: UserService
  ) { 
    this.perfil = this._userService.getPerfil();
    this.p = global.perfiles;
    this.ruta = global.ruta;
  }
  
  ngOnInit() {
    // Conseguir elemento
    this.action = localStorage.getItem("action");
    if(this.action == "ver" || this.action == "editar"){
      this.onSubmit();
    }
    this.dtOptions = {
      language: {
        "decimal": "",
        "emptyTable": "",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "",
            "previous": ""
        }
      }
    };
  }

  onSubmit(){
    this.posts = [
      {
        "id": 170800186,
        "rbd": "4791",
        "cantidad": "$ 9.877.105",
        "date": "04/11/2017"
      }
    ];
    this.status = "success";

  };
  EliminaRegistro(){
    this.posts = [];
    this.status = "delete";    
  }

}
