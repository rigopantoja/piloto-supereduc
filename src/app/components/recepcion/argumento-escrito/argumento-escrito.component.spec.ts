import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgumentoEscritoComponent } from './argumento-escrito.component';

describe('ArgumentoEscritoComponent', () => {
  let component: ArgumentoEscritoComponent;
  let fixture: ComponentFixture<ArgumentoEscritoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArgumentoEscritoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgumentoEscritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
