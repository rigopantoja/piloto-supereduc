import { Component, OnInit } from '@angular/core';
import { global } from '../../../services/global';

@Component({
  selector: 'app-argumento-escrito',
  templateUrl: './argumento-escrito.component.html',
  styleUrls: ['./argumento-escrito.component.css']
})
export class ArgumentoEscritoComponent implements OnInit {

  public argumento;
  public ruta;
  public action;

  constructor() { 
    this.argumento = localStorage.getItem('argumento');
    this.ruta = global.ruta;
  }

  ngOnInit() {
    // Conseguir elemento
    this.action = localStorage.getItem("action");
  }

  onSubmit(form){
    
    localStorage.setItem('argumento', JSON.stringify(this.argumento));
  }

}
