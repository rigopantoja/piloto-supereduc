import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentacionRecursoComponent } from './presentacion-recurso.component';

describe('PresentacionRecursoComponent', () => {
  let component: PresentacionRecursoComponent;
  let fixture: ComponentFixture<PresentacionRecursoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresentacionRecursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentacionRecursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
