import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirEscritoComponent } from './subir-escrito.component';

describe('SubirEscritoComponent', () => {
  let component: SubirEscritoComponent;
  let fixture: ComponentFixture<SubirEscritoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubirEscritoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubirEscritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
