import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { global } from '../../../services/global';

@Component({
  selector: 'app-subir-escrito',
  templateUrl: './subir-escrito.component.html',
  styleUrls: ['./subir-escrito.component.css'],
  providers: [UserService]
})
export class SubirEscritoComponent implements OnInit {
  public perfil: string;
  public p;
  public ruta;
  public action;

  constructor(
    public _userService: UserService
  ) { 
    this.perfil = this._userService.getPerfil();
    this.p = global.perfiles;
    this.ruta = global.ruta;
  }

    /* DATOS TABLA */
    posts = [
      {
        "formato": "Cajas"
      },
      {
        "formato": "CD"
      },
      {
        "formato": "Pendrives"
      },
      {
        "formato": "Archivadores"
      }
  ];

  ngOnInit() {
    // Conseguir elemento
    this.action = localStorage.getItem("action");
  }

}
