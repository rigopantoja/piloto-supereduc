import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { global } from '../../../services/global';
import { Action } from '../../../models/action';


@Component({
  selector: 'app-listar-recursos',
  templateUrl: './listar-recursos.component.html',
  styleUrls: ['./listar-recursos.component.css'],
  providers: [UserService]
})
export class ListarRecursosComponent implements OnInit {

  dtOptions: DataTables.Settings = {};

  public perfil: string;
  public p;
  public th1;public th2;public th3;public th4;public th5;public th6;public th7;public th8;public th9;public th10;public th11;public th12;public th13;public th14;public th15;
  public th1_t2;public th2_t2;public th3_t2;public th4_t2;public th5_t2;public th6_t2;public th7_t2;public th8_t2;public th9_t2;public th10_t2;public th11_t2;public th12_t2;public th13_t2;public th14_t2;public th15_t2;
  public th1_t3;public th2_t3;public th3_t3;public th4_t3;public th5_t3;public th6_t3;public th7_t3;public th8_t3;public th9_t3;public th10_t3;public th11_t3;public th12_t3;public th13_t3;public th14_t3;public th15_t3;
  
  public ruta;
  public action: Action;


    

  constructor(
    public _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { 

    this.perfil = this._userService.getPerfil();
    this.p = global.perfiles;
    this.ruta = global.ruta;
    this.action = new Action ('', '', '');
    
    
    if (this.perfil == this.p.p1 || this.perfil == this.p.p2 || this.perfil == this.p.p3 || this.perfil == this.p.p4 || this.perfil == this.p.p5 || this.perfil == this.p.p6 || this.perfil == this.p.p7 || this.perfil == this.p.p8 || this.perfil == this.p.p9){
      this.th8=true; this.th9=true; this.th12=true; this.th14=true; 
      this.th2_t2=true; this.th5_t2=true; this.th8_t2=true; this.th12_t2=true; this.th14_t2=true; this.th15_t2=true;
      this.th2_t3=true; this.th5_t3=true; this.th8_t3=true; this.th9_t3=true; this.th11_t3=true; this.th12_t3=true; this.th14_t3=true; this.th15_t3=true;
    }
    if (this.perfil == this.p.p2 || this.perfil == this.p.p3 || this.perfil == this.p.p4 || this.perfil == this.p.p5 || this.perfil == this.p.p6 || this.perfil == this.p.p7 || this.perfil == this.p.p8 || this.perfil == this.p.p9){
      this.th3=true; this.th5=true;
      this.th3_t2=true; this.th6_t2=true;
      this.th3_t3=true; this.th6_t3=true; this.th13_t3=true;
    }
    if (this.perfil == this.p.p3 || this.perfil == this.p.p4 || this.perfil == this.p.p5 || this.perfil == this.p.p6 || this.perfil == this.p.p7 || this.perfil == this.p.p8 || this.perfil == this.p.p9){
      this.th2=true; this.th6=true; this.th7=true; this.th10=true; this.th15=true;
      this.th7_t2=true; this.th10_t2=true; 
      this.th7_t3=true;  this.th10_t3=true;
    }
    if (this.perfil == this.p.p1 || this.perfil == this.p.p3 || this.perfil == this.p.p4 || this.perfil == this.p.p5 || this.perfil == this.p.p6 || this.perfil == this.p.p7 || this.perfil == this.p.p8 || this.perfil == this.p.p9){
      
      this.th9_t2=true;
    }
    if (this.perfil == this.p.p2 || this.perfil ==this.p.p3 || this.perfil ==this.p.p4 || this.perfil ==this.p.p5 || this.perfil ==this.p.p6){
      
      this.th13_t2=true;
    }
    if (this.perfil == this.p.p1 || this.perfil == this.p.p2 || this.perfil == this.p.p9){
      this.th4=true;
    }
    if (this.perfil == this.p.p3 || this.perfil == this.p.p4){
      this.th13=true;
    }

    if (this.perfil == " "){
      this.th11=true;
      this.th4_t2=true; this.th11_t2=true;
      this.th4_t3=true;
    }
  }

  /* DATOS TABLA BORRADOR/MIS PENDIENTES */
  posts_borrador = [
      {
        "id": '1-2019',
        "rol_recurso": "170800373",
        "rut_sostenedor": "65.154.413-0",
        "Fecha_Inicio": "02/01/2019",
        "Fecha_Presentacion": "07/04/2017",
        "Fecha_Ingreso": "07/04/2017",
        "Dias_Tramitacion": "",
        "Estado_Recurso": "Borrador",
        "Fecha_Estado": "04/01/2019",
        "Prioridad": "ALTA",
        "Fecha_Resolucion": "",
        "Tipo_Recurso": "Reposición",
        "Fiscal_Encargado": "FMP",
        "Programa": "SEP 2015",
        "ControlIngreso": ""
      },
      {
        "id": '2-2019',
        "rol_recurso": "170800186",
        "rut_sostenedor": "65.154.015-1",
        "Fecha_Inicio": "10/02/2019",
        "Fecha_Presentacion": "07/04/2017",
        "Fecha_Ingreso": "07/04/2017",
        "Dias_Tramitacion": "",
        "Estado_Recurso": "Borrador",
        "Fecha_Estado": "14/02/2019",
        "Prioridad": "MEDIA",
        "Fecha_Resolucion": "",
        "Tipo_Recurso": "Reposición",
        "Fiscal_Encargado": "FMP",
        "Programa": "SEP 2015",
        "ControlIngreso": ""
      },
      {
        "id": '3-2019',
        "rol_recurso": "181300981",
        "rut_sostenedor": "65.151.013-9",
        "Fecha_Inicio": "04/02/2019",
        "Fecha_Presentacion": "02/03/2018",
        "Fecha_Ingreso": "02/03/2018",
        "Dias_Tramitacion": "",
        "Estado_Recurso": "Borrador",
        "Fecha_Estado": "04/02/2019",
        "Prioridad": "MEDIA",
        "Fecha_Resolucion": "",
        "Tipo_Recurso": "Reposición",
        "Fiscal_Encargado": "ERC",
        "Programa": "SEP 2015",
        "ControlIngreso": ""
      },
      {
        "id": '4-2019',
        "rol_recurso": "180100041",
        "rut_sostenedor": "65.099.420-5",
        "Fecha_Inicio": "07/03/2019",
        "Fecha_Presentacion": "05/03/2018",
        "Fecha_Ingreso": "05/03/2018",
        "Dias_Tramitacion": "07/02/2019",
        "Estado_Recurso": "Borrador",
        "Fecha_Estado": "07/02/2019",
        "Prioridad": "MEDIA",
        "Fecha_Resolucion": "",
        "Tipo_Recurso": "Invalidar",
        "Fiscal_Encargado": "ERC",
        "Programa": "SEP 2016",
        "ControlIngreso": ""
      },
      {
        "id": '5-2019',
        "rol_recurso": "170500212",
        "rut_sostenedor": "65.151.677-9",
        "Fecha_Inicio": "08/06/2019",
        "Fecha_Presentacion": "17/10/2018",
        "Fecha_Ingreso": "17/10/2018",
        "Dias_Tramitacion": "",
        "Estado_Recurso": "Pendiente",
        "Fecha_Estado": "08/02/2019",
        "Prioridad": "MEDIA",
        "Fecha_Resolucion": "",
        "Tipo_Recurso": "Revocación",
        "Fiscal_Encargado": "DMB",
        "Programa": "SEP 2015",
        "ControlIngreso": ""
      }
  ];

  /* DATOS TABLA EN PROCESO */
  posts_proceso = [
    {
      "id": 170800373,
      "rol_recurso": "E-13836",
      "rut_sostenedor": "65.154.413-0",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "07/04/2017",
      "Fecha_Ingreso": "07/04/2017",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Pendiente",
      "Fecha_Estado": "",
      "Prioridad": "ALTA",
      "Fecha_Resolucion": "",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "FMP",
      "Programa": "SEP 2015",
      "ControlIngreso": ""
    },
    {
      "id": 170800186,
      "rol_recurso": "E-27623",
      "rut_sostenedor": "65.154.015-1",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "07/04/2017",
      "Fecha_Ingreso": "07/04/2017",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Pendiente",
      "Fecha_Estado": "",
      "Prioridad": "MEDIA",
      "Fecha_Resolucion": "",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "FMP",
      "Programa": "SEP 2015",
      "ControlIngreso": ""
    },
    {
      "id": 181300981,
      "rol_recurso": "E-31305",
      "rut_sostenedor": "65.151.013-9",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "02/03/2018",
      "Fecha_Ingreso": "02/03/2018",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Pendiente",
      "Fecha_Estado": "",
      "Prioridad": "MEDIA",
      "Fecha_Resolucion": "",
      "Tipo_Recurso": "Jerárquico en subsidio",
      "Fiscal_Encargado": "ERC",
      "Programa": "SEP 2015",
      "ControlIngreso": ""
    },
    {
      "id": 180100041,
      "rol_recurso": "E-30259",
      "rut_sostenedor": "65.099.420-5",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "05/03/2018",
      "Fecha_Ingreso": "05/03/2018",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Pendiente",
      "Fecha_Estado": "",
      "Prioridad": "MEDIA",
      "Fecha_Resolucion": "",
      "Tipo_Recurso": "Jerárquico en subsidio",
      "Fiscal_Encargado": "ERC",
      "Programa": "SEP 2016",
      "ControlIngreso": ""
    },
    {
      "id": 170500212,
      "rol_recurso": "E-35705",
      "rut_sostenedor": "65.151.677-9",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "17/10/2018",
      "Fecha_Ingreso": "17/10/2018",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Pendiente",
      "Fecha_Estado": "",
      "Prioridad": "MEDIA",
      "Fecha_Resolucion": "",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "DMB",
      "Programa": "SEP 2015",
      "ControlIngreso": ""
    }
  ];

  /* DATOS TABLA TERMINADO */
  posts = [
    {
      "id": 170800372,
      "rol_recurso": "E-13587",
      "rut_sostenedor": "65.799.980-6",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "8/5/2017",
      "Fecha_Ingreso": "8/5/2017",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Terminado",
      "Fecha_Estado": "",
      "Prioridad": "MEDIA",
      "Fecha_Resolucion": "28/3/2019",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "FMP",
      "Programa": "SEP 2015",
      "ControlIngreso": ""
    },
    {
      "id": 170700945,
      "rol_recurso": "E-4002",
      "rut_sostenedor": "72.725.600-8",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "2/8/2017",
      "Fecha_Ingreso": "2/8/2017",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Terminado",
      "Fecha_Estado": "",
      "Prioridad": "MEDIA",
      "Fecha_Resolucion": "28/3/2019",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "FMP",
      "Programa": "SEP 2013-2014",
      "ControlIngreso": ""
    },
    {
      "id": 180500012,
      "rol_recurso": "E-3148",
      "rut_sostenedor": "65.152.916-6",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "12/12/2017",
      "Fecha_Ingreso": "12/12/2017",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Terminado",
      "Fecha_Estado": "",
      "Prioridad": "MEDIA",
      "Fecha_Resolucion": "5/12/2018",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "ERC",
      "Programa": "SEP 2016",
      "ControlIngreso": ""
    },
    {
      "id": 180200003,
      "rol_recurso": "E-3432",
      "rut_sostenedor": "70.047.302-3",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "12/12/2017",
      "Fecha_Ingreso": "12/12/2017",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Terminado",
      "Fecha_Estado": "",
      "Prioridad": "BAJA",
      "Fecha_Resolucion": "26/12/2018",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "ERC",
      "Programa": "SEP 2016",
      "ControlIngreso": ""
    },
    {
      "id": 170902086,
      "rol_recurso": "E-1566",
      "rut_sostenedor": "65.120.472-0",
      "Fecha_Inicio": " ",
      "Fecha_Presentacion": "11/12/2017",
      "Fecha_Ingreso": "11/12/2017",
      "Dias_Tramitacion": "",
      "Estado_Recurso": "Terminado",
      "Fecha_Estado": "",
      "Prioridad": "ALTA",
      "Fecha_Resolucion": "5/7/2018",
      "Tipo_Recurso": "Jerárquico",
      "Fiscal_Encargado": "DMB",
      "Programa": "PIE 2015",
      "ControlIngreso": ""
    }
  ];

  

  ngOnInit() {
/*     this.dtOptions = {
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }

      },
      dom: 'Bfrtip',
      buttons: [
            'csv', 'excel', 'pdf', 'print'
      ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    }; */

    this.dtOptions = {
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
      }
    };

  }

  actionTable(ver){
    localStorage.setItem('action', ver);
    this._router.navigate(['/definicion-recurso']);
  }
}
