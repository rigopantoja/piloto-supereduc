import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgumentoGastosComponent } from './argumento-gastos.component';

describe('ArgumentoGastosComponent', () => {
  let component: ArgumentoGastosComponent;
  let fixture: ComponentFixture<ArgumentoGastosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArgumentoGastosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgumentoGastosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
