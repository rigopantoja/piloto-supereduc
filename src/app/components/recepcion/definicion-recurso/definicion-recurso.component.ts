import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { global } from '../../../services/global';
import json from "../../../../assets/json/recurso.json";

@Component({
  selector: 'app-definicion-recurso',
  templateUrl: './definicion-recurso.component.html',
  styleUrls: ['./definicion-recurso.component.css'],
  providers: [UserService]
})
export class DefinicionRecursoComponent implements OnInit {

  public status;
  public perfil: string;
  public p;
  public opcionSeleccionado: string  = 'Artículo 60 letra a) ley 19880';
  public verSeleccion: string        = 'Que la resolución se hubiere dictado sin el debido emplazamiento';
  public fundamentacion: string;
  public ruta;
  public action;
  public _tipoRecursoPrincipal;
json: any = json;
  

  constructor(
    public _userService: UserService
  ) { 
    this.perfil = this._userService.getPerfil();
    this.p = global.perfiles;
    this.ruta = global.ruta;

  }

  capturar(){
    if(this.opcionSeleccionado == "Artículo 60 letra a) ley 19880"){
      this.verSeleccion = "Que la resolución se hubiere dictado sin el debido emplazamiento";

    }
    if(this.opcionSeleccionado == "Artículo 60 letra b) ley 19880"){
      this.verSeleccion = "Que, al dictarlo, se hubiere incurrido en manifiesto error de hecho y que éste haya sido determinante para la decisión adoptada, o que aparecieren documentos de valor esencial para la resolución del asunto, ignorados al dictarse el acto o que no haya sido posible acompañarlos al expediente administrativo en aquel momento";
    }
    if(this.opcionSeleccionado == "Artículo 60 letra c) ley 19880"){
      this.verSeleccion = "Que por sentencia ejecutoriada se haya declarado que el acto se dictó como consecuencia de prevaricación, cohecho, violencia u otra maquinación fraudulenta";
    }
    if(this.opcionSeleccionado == "Artículo 60 letra d) ley 19880"){
      this.verSeleccion = "Que en la resolución hayan influido de modo esencial documentos o testimonios declarados falsos por sentencia ejecutoriada posterior a aquella resolución, o que siendo anterior, no hubiese sido conocida oportunamente por el interesado";
    }

  }

  ngOnInit() {
    // Conseguir elemento
    this.action = localStorage.getItem("action");
    console.log(this.action);

  }

  onSubmit(form){
    this.status = "success";
  }
  submitCausal(forma){
    this.status = "success";
  }
  reset(){
    //this.formValues.resetForm();
    this.status = "error";
  }



}
