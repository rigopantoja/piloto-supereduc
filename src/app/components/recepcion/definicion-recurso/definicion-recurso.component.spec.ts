import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinicionRecursoComponent } from './definicion-recurso.component';

describe('DefinicionRecursoComponent', () => {
  let component: DefinicionRecursoComponent;
  let fixture: ComponentFixture<DefinicionRecursoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefinicionRecursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinicionRecursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
