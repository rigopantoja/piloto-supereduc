import { Component, OnInit } from '@angular/core';
import { global } from '../../../services/global';

@Component({
  selector: 'app-antecedentes',
  templateUrl: './antecedentes.component.html',
  styleUrls: ['./antecedentes.component.css']
})
export class AntecedentesComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  public ruta;
  public action;

  constructor() { 
    this.ruta = global.ruta;
  }

  posts = [
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "Patricia Paula Rodríguez Cabrera"
    }


  ];

  postsFile = [
    {
      "doc": "Remu_08_2018.doc"
    },
    {
      "doc": "Documentacion_legal_contractual.doc"
    },
    {
      "doc": "RemuneracioResponsabilidades1.pdf"
    },
    {
      "doc": "RemuneracioResponsabilidades2.pdf"
    }


  ];

  postsArgumento= [
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "9817456-3",
      "categoria": "",
      "n_doc": "12444",
      "periodo": "Junio 2016",
      "cuenta": "Cta 410 104 INCREMENTO % ZONA",
      "montono": "$ 200.000",
      "montoimpugnado": "$ 200.000"
    },
    {
      "id": 123,
      "RBD": "4791",
      "trabajador": "9817456-3",
      "categoria": "",
      "n_doc": "91239",
      "periodo": "Julio 2016",
      "cuenta": "Cta 410 101 SUELDO BASE",
      "montono": "$ 402.000",
      "montoimpugnado": "$ 402.000"
    }


  ];

  
  ngOnInit() {
    // Conseguir elemento
    this.action = localStorage.getItem("action");

    this.dtOptions = {
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
      }
    };
  }

}
