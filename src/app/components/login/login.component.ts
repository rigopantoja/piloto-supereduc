import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {User} from '../../models/user';
import { global } from '../../services/global'; // Aqui tengo variables globales para simular  BBDD

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  public user: User;
  public intentity;
  public dato;
  public perfil;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute
  ) { 
    this.user = new User (1, '', '', 'ROLE_USER', '', '', '', '');
    this.dato = global.intentity;
  }

  ngOnInit() {
    this.logout();
  }

  onSubmit(form){
    
    
    this.intentity = this.dato;
    console.log(this.user.password);
    console.log(this.intentity);

    

    // PERSISTIR DATOS USUARIO IDENTIFICADO
    //localStorage.setItem('identity', this.intentity);
    localStorage.setItem('identity', JSON.stringify(this.intentity));
    //sessionStorage.setItem('identity', JSON.stringify(this.intentity));
    localStorage.setItem('name', JSON.stringify(this.user.name));
    localStorage.setItem('perfil', JSON.stringify(this.user.password));
    //localStorage.setItem('identity', JSON.stringify (this.intentity));

    // Redirección al bienvenido
    this._router.navigate(['/bienvenida']);

  }

  logout(){
    this._route.params.subscribe(params => {
      let logout = +params['sure'];

      if(logout == 1){
        localStorage.removeItem('identity');
        //sessionStorage.removeItem('identity');
        localStorage.removeItem('perfil');

        //this.identity = null;

        // Redirección al inicio
        this._router.navigate(['login']);
      }
    });
  }

}
