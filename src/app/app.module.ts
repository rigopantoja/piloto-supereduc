import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // PARA USAR FORMULARIO ANGULAR
import { HttpClientModule } from '@angular/common/http';
import { routing, appRoutingProviders } from './app-routing.module';
import { DataTablesModule } from 'angular-datatables';


//import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarRecursosComponent } from './components/recepcion/listar-recursos/listar-recursos.component';
import { PresentacionRecursoComponent } from './components/recepcion/presentacion-recurso/presentacion-recurso.component';
import { LoginComponent } from './components/login/login.component';
import { BienvenidaComponent } from './components/bienvenida/bienvenida.component';
import { DefinicionRecursoComponent } from './components/recepcion/definicion-recurso/definicion-recurso.component';
import { GestionarActaRecursoComponent } from './components/recepcion/gestionar-acta-recurso/gestionar-acta-recurso.component';
import { ImpugnarGastosComponent } from './components/recepcion/impugnar-gastos/impugnar-gastos.component';
import { ArgumentoGastosComponent } from './components/recepcion/argumento-gastos/argumento-gastos.component';
import { AntecedentesComponent } from './components/recepcion/antecedentes/antecedentes.component';
import { ArgumentoEscritoComponent } from './components/recepcion/argumento-escrito/argumento-escrito.component';
import { SubirEscritoComponent } from './components/recepcion/subir-escrito/subir-escrito.component';
import { DatatableComponent } from './components/recepcion/datatable/datatable.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    ListarRecursosComponent,
    PresentacionRecursoComponent,
    LoginComponent,
    BienvenidaComponent,
    DefinicionRecursoComponent,
    GestionarActaRecursoComponent,
    ImpugnarGastosComponent,
    ArgumentoGastosComponent,
    AntecedentesComponent,
    ArgumentoEscritoComponent,
    SubirEscritoComponent,
    DatatableComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DataTablesModule
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
