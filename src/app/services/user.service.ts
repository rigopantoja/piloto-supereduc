import { Injectable } from '@angular/core';

@Injectable()
export class UserService{

    public identity;
    public perfil;

    constructor(

    ){

    }

    getIdentity(){
        let identity = localStorage.getItem('identity');

        if(identity && identity != 'undefined'){
            this.identity = JSON.parse(identity);
        }else{
            this.identity = null;
        }

        return this.identity;
    }
    getPerfil(){
        let perfil = localStorage.getItem('perfil');

        if(perfil && perfil != 'undefined'){
            this.perfil = JSON.parse(perfil);
        }else{
            this.perfil = null;
        }

        return this.perfil;
    }

}