export var global = {
    login: 'ok',
    perfiles:   {
                "p1": "Sostenedor",
                "p2": "ApoyoAdmin",
                "p3": "EncFiscalia", /* Enc jurídico (DR), Enc Plur (DN) */
                "p4": "EncFiscalizacion",
                "p5": "FiscalRedactor",
                "p6": "Fiscalizador",
                "p7": "Jefe",
                "p8": "DirRegional",
                "p9": "Fiscal",
                "p10": "OficinaPartes"
                },
    intentity: {"name":"Rodrigo","role":"admin"},
    ruta: 'assets/'
    

}