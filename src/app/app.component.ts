import { Component, OnInit, DoCheck } from '@angular/core'; // OnInit y DoCheck para actualizar los cambios del localstorage
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})
export class AppComponent implements OnInit, DoCheck {

  public title = 'prototipo';
  public identity;
  public perfil;
  public name;

  constructor(
    public _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute
  ){
    this.identity = this._userService.getIdentity();
    this.perfil = this._userService.getPerfil();
    this.name = localStorage.getItem('name');
  }

  ngOnInit(){
    console.log("cargada");
    // Elimina localStorage al cerrar ventana GENERA PROBLEMAS AL REFRESCAR PÁGINA
    //window.localStorage.removeItem("identity");
    

  }
  

  ngDoCheck(){
    this.identity = this._userService.getIdentity();
    this.perfil = this._userService.getPerfil();
    this.name = localStorage.getItem('name');
  }

  actionTable(ver){
    localStorage.setItem('action', ver);
    this._router.navigate(['/definicion-recurso']);
  }

}
